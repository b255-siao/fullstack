import './App.css';

import { Fragment } from 'react'

import Login from './pages/Login'
import Logout from './pages/Logout'
import Register from './pages/Register'

function App() {
  return (
    <Fragment>
      <Register />
      <Login />
      <Logout />
    </Fragment>
  )
}

export default App;
