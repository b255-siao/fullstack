function countLetter(letter, sentence) {
  let result = 0;

  // Check first whether the letter is a single character.
  // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
  // If letter is invalid, return undefined.

  for(let i = 0; i < sentence.length; i++) {
    if(sentence.charAt(i) == letter) {
      result += 1
    } else {
      console.log('letter is invalid')
    }
  }
  console.log(result)
}
countLetter('b', 'kingsolomonsiao')
countLetter('b', 'bulk')



function isIsogram(text) {
  // An isogram is a word where there are no repeating letters.
  // The function should disregard text casing before doing anything else.
  // If the function finds a repeating letter, return false. Otherwise, return true.

  str = text.toLowerCase()
  for (var i = 0; i < str.length; i++) {
    if (str.indexOf(str.charAt(i), i + 1) >= 0) {
      console.log(false)
    }
  } 
  console.log(true)
}
isIsogram("king")
isIsogram("kingkong")



function purchase(age, price) {
  // Return undefined for people aged below 13.
  // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
  // Return the rounded off price for people aged 22 to 64.
  // The returned value should be a string.
  
  return age < 13 ? "undefined" 
         : age >= 13 && age <= 21 ? "Discounted Price: " + String(Math.round(price - (price * .2)))
         : age >= 22 && age <= 64 ? "Discounted Price: " + String(Math.round(price))
         : "Discounted Price: " + String(Math.round(price - (price * .2)))
}
console.log(purchase(10, 100))
console.log(purchase(15, 100))
console.log(purchase(25, 100))
console.log(purchase(65, 100))



function findHotCategories(items) {
  // Find categories that has no more stocks.
  // The hot categories must be unique; no repeating categories.

  // The expected output after processing the items array is ['toiletries', 'gadgets'].
  // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.

  let hotItems = []
  items.forEach(item => {
    if(item.stocks === 0) {
      hotItems.push(item.category) 
    }
  })
  hotItems = hotItems.filter(
    (item, index, hotItems) => hotItems.indexOf(item) === index
  )

  return hotItems
}
const items = [
  { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' },
  { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' },
  { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' },
  { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' },
  { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
]
findHotCategories(items)



function findFlyingVoters(candidateA, candidateB) {
  // Find voters who voted for both candidate A and candidate B.

  // The passed values from the test are the following:
  

  // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
  // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.

  const voters = []

  candidateA.forEach(votes => {
    if(candidateB.includes(votes)) {
      voters.push(votes)
    }
  })
  console.log(voters)
}
findFlyingVoters(
  ["LIWf1l", "V2hjZH", "rDmZns", "PvaRBI", "i7Xw6C", "NPhm2m"],
  ["kcUtuu", "LLeUTl", "r04Zsl", "84EqYo", "V2hjZH", "LIWf1l"]
)


module.exports = {
  countLetter,
  isIsogram,
  purchase,
  findHotCategories,
  findFlyingVoters
};
