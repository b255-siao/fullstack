let collection = [];

// Write the queue functions below.
function print() {
    if(collection.length === 0) {
        return []
    }
}
print()


function enqueue(array) {
    collection.unshift(array)
    return collection
}
enqueue()

function dequeue() {
    collection.shift()
    return collection
}
dequeue()

function front() {
    return collection[0]
}
front()


function size() {
    return collection.length
}
size()


function isEmpty() {
    if(!collection.length) {
        return true
    } else {
        return false
    }
}
isEmpty()

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};